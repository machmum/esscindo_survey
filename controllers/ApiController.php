<?php
/**
 * Copyright (C) Alfat Saputra Harun - All Rights Reserved
 *
 * @since 1.0
 * @author harunalfat <harunalfat@gmail.com>
 * @copyright Copyright (c) 2016
 * @link http://blog.harunalfat.com/
 */

 namespace app\controllers;

 use Yii;
 use yii\filters\AccessControl;
 use yii\web\Controller;
 use yii\web\Response;
 use yii\web\NotFoundHttpException;
 use yii\helpers\Json;
 use yii\helpers\Html;
 use yii\filters\ContentNegotiator;
 use yii\filters\auth\CompositeAuth;
 use yii\filters\auth\HttpBasicAuth;
 use yii\filters\auth\HttpBearerAuth;
 use yii\filters\auth\QueryParamAuth;
 use app\events\SubmissionEvent;
 use app\components\analytics\Analytics;
 use app\modules\user\models\User;
 use app\models\Form;
 use app\models\FormSubmission;
 use app\models\FormSubmissionFile;
 use app\models\Theme;
 use app\models\forms\RestrictedForm;

 class ApiController extends Controller
 {

   /**
    * @inheritdoc
    */
   public $defaultAction = 'form';

   /**
    * @event SubmissionEvent an event fired when a submission is received.
    */
   const EVENT_SUBMISSION_RECEIVED = 'app.form.submission.received';

   /**
    * @event SubmissionEvent an event fired when a submission is accepted.
    */
   const EVENT_SUBMISSION_ACCEPTED = 'app.form.submission.accepted';

   /**
    * @event SubmissionEvent an event fired when a submission is rejected by validation errors.
    */
   const EVENT_SUBMISSION_REJECTED = 'app.form.submission.rejected';


    public function behaviors()
    {
       $behaviors = parent::behaviors();
       $behaviors['authenticator'] = [
           'class' => HttpBearerAuth::className()
       ];
      $behaviors['access'] = [
          'class' => AccessControl::className(),
          'only' => ['forms'],
          'rules' => [
              [
                  'actions' => ['forms'],
                  'allow' => true,
                  'roles' => ['@'],
              ],
          ],
      ];
      $behaviors['contentNegotiator'] = [
          'class' => ContentNegotiator::className(),
          'formats' => [
              'application/json' => Response::FORMAT_JSON,
          ]
      ];
       return $behaviors;
    }

    public function beforeAction($action){
      $this->enableCsrfValidation = false;
      return parent::beforeAction($action);
    }

   public function actionForms()
   {
     Yii::$app->response->format = Response::FORMAT_JSON;

     $headers = Yii::$app->response->headers;
     $headers->add('Content-Type', 'application/json');

     $assigned_form_ids = Yii::$app->user->getAssignedFormIds();

     $assigned_forms = Form::findAll($assigned_form_ids);

     return $assigned_forms;
   }

   public function actionForm($id)
   {
     Yii::$app->response->format = Response::FORMAT_JSON;

     $headers = Yii::$app->response->headers;
     $headers->add('Content-Type', 'application/json');

     $formModel = $this->findFormModel($id);

     $response = array();
     $response["id"] = $formModel->id;

     $all_fields = json_decode($formModel->formData->fields, true);
     foreach ($all_fields as $key => $all_field){
       if (isset($all_field["required"])){
         if ($all_field["required"] == true){
           $all_fields[$key]["required"] = 1;
         } else{
           $all_fields[$key]["required"] = 0;
         }
       }
     }

     $html_fields = $this->decodeHtml($formModel->formData->html);


     $response["fields"] = $all_fields;

     if (isset($html_fields)){
       foreach ($html_fields as $html_field) {
         array_push($response["fields"], $html_field);
       }
     }

     $formRules = $formModel->formRules;

     if (isset($formRules)){

       $rules = array();
       foreach ($formRules as $formRule){
         $conditions = null;
         $actions = null;
         $actionFields = array();
         if (isset($formRule->conditions)){
           $conditions =  json_decode($formRule->conditions);
           $rule["conditions"] = $conditions;
         }

         if (isset($formRule->actions)){
           $actions =  json_decode($formRule->actions);
           foreach ($actions as $action){
             $actionFields = $action->fields;
             $fields = array();
             foreach ($actionFields as $actionField){

               if (isset($actionField->fields)){
               	  $actionFieldFields = $actionField->fields;
      	          foreach ($actionFieldFields as $actionFieldField) {
      	             array_push($fields,$actionFieldField);
      	          }
               } else {
                  array_push($fields, $actionField);
               }

             }
             $action->fields = $fields;
           }
           $rule["actions"] = $actions;
         }

         if (isset($rule)){
           array_push($rules,$rule);
         }

       }
       $response["rules"] = $rules;

     }

     return $response;
   }

   public function actionSubmit($id)
   {
     // The HTTP post request
     $post = Yii::$app->request->post();

     if (isset($post)) {

         /**************+++++++++++++++++
         /* Prepare response by default
         /*******************************/
         $formModel = $this->findFormModel($id);

         // Response fornat
         Yii::$app->response->format = Response::FORMAT_JSON;

         // Default response
         $response = array(
             'action'  => 'submit',
             'success' => true,
             'id' => 0,
             'message' => Yii::t('app', 'Your message has been sent.'),
         );

         /**************+++++++++++++++++
         /* Prepare data
         /*******************************/

         // Set public scenario of the submission
         $formSubmissionModel = new FormSubmission(['scenario' => 'public']);

         /** @var \app\models\FormData $formDataModel */
         $formDataModel = $formModel->formData;
         // Get all fields except buttons and files
         $fields = $formDataModel->getFieldsWithoutFilesAndButtons();
         // Get file fields
         $fileFields = $formDataModel->getFileFields();

         // Remove fields with null values and
         // Strip whitespace from the beginning and end of each post value
         $submissionData = $formSubmissionModel->cleanSubmission($fields, $post);
         // Get uploaded files
         $files = $formSubmissionModel->getUploadedFiles($fileFields);
         // File paths cache
         $filePaths = array();

         // Prepare Submission for validation
         $postFormSubmission = [
             'FormSubmission' => [
                 'form_id' => $formModel->id, // Form Model id
                 'data' => $submissionData, // (array)
             ]
         ];

         /**************+++++++++++++++++
         /* FormSubmission Validation
         /*******************************/

         if ($formSubmissionModel->load($postFormSubmission) && $formSubmissionModel->validate()) {

             Yii::$app->trigger($this::EVENT_SUBMISSION_RECEIVED, new SubmissionEvent([
                 'sender' => $this,
                 'form' => $formModel,
                 'submission' => $formSubmissionModel,
                 'files' => $files,
             ]));

             if ($formModel->saveToDB()) {

                 /**************+++++++++++++++++
                 /* Save to DB
                 /*******************************/

                 // Save submission in single transaction
                 $transaction = Form::getDb()->beginTransaction();

                 try {

                     // Save submission without validation
                     if ($formSubmissionModel->save(false)) {

                         // Save files to DB and disk

                         /* @var $file \yii\web\UploadedFile */
                         foreach ($files as $file) {
                             if (isset($file)) {
                                 // Save file to DB
                                 $fileModel = new FormSubmissionFile();
                                 $fileModel->submission_id = $formSubmissionModel->primaryKey;
                                 $fileModel->form_id = $formModel->id;
                                 // Replace special characters before the file is saved
                                 $fileModel->name = preg_replace("/[^a-zA-Z0-9]/", "", $file->baseName) .
                                     "-" .
                                     $formSubmissionModel->primaryKey;
                                 $fileModel->extension = $file->extension;
                                 $fileModel->size = $file->size;
                                 $fileModel->status = 1;
                                 $fileModel->save();

                                 // Throw exception if validation fail
                                 if (isset($fileModel->errors) && count($fileModel->errors) > 0) {
                                     throw new \Exception(Yii::t("app", "Error saving files."));
                                 }

                                 // Save file to disk
                                 $filePath = $fileModel->getFilePath();
                                 $file->saveAs($filePath);
                                 array_push($filePaths, $filePath);
                             }
                         }

                         // Change response id
                         $response["id"] = $formSubmissionModel->primaryKey;

                     }

                     $transaction->commit();

                 } catch (\Exception $e) {
                     // Rolls back the transaction
                     $transaction->rollBack();
                     // Rethrow the exception
                     throw $e;
                 }

             } else {

                 /**************+++++++++++++++++
                 /* Don't save to DB
                 /*******************************/

                 // Save files to disk
                 foreach ($files as $file) {
                     /* @var $file \yii\web\UploadedFile */
                     if (isset($file)) {
                         $filePath = $formModel::FILES_DIRECTORY . DIRECTORY_SEPARATOR . $formModel->id .
                             DIRECTORY_SEPARATOR . $file->name;
                         $file->saveAs($filePath);
                         array_push($filePaths, $filePath);
                     }
                 }
             }

             Yii::$app->trigger($this::EVENT_SUBMISSION_ACCEPTED, new SubmissionEvent([
                 'sender' => $this,
                 'form' => $formModel,
                 'submission' => $formSubmissionModel,
                 'files' => $files,
                 'filePaths' => $filePaths,
             ]));

         } else {

             Yii::$app->trigger($this::EVENT_SUBMISSION_REJECTED, new SubmissionEvent([
                 'sender' => $this,
                 'form' => $formModel,
                 'submission' => $formSubmissionModel,
             ]));

             // Print validation errors
             $errors = array();
             foreach ($formSubmissionModel->errors as $field => $messages) {
                 array_push($errors, array(
                     "field" => $field,
                     "messages" => $messages,
                 ));
             }

             // Change response
             $response["success"] = false;
             $response["message"] = Yii::t('app', 'There is {startTag}an error in your submission{endTag}.', [
                 'startTag' => '<strong>',
                 'endTag' => '</strong>',
             ]);
             $response["errors"] = $errors;

         }

         return $response;

     }
   }

   protected function findFormModel($id)
   {
       if (($model = Form::findOne($id)) !== null) {
           return $model;
       } else {
           throw new NotFoundHttpException(Yii::t("app", "The requested page does not exist."));
       }
   }

   private function decodeHtml($html_encoded)
   {
      $html_decoded = html_entity_decode($html_encoded);
      $dom = new \DOMDocument;
      $dom->preserveWhiteSpace = false;
      $dom->loadHTML($html_decoded);
      $xpath = new \DOMXpath($dom);

      $paragraphs = $xpath->query("//*[@id=\"form-app\"]/fieldset/p/strong/span[2]");
      if (!is_null($paragraphs)){
        return $this->jsonParagraph($paragraphs);
      }

      return null;
   }

   private function jsonParagraph($paragraphs){
     $paragraph_array = array();
     foreach ($paragraphs as $paragraph) {
       $paragraph = $paragraph->parentNode;

       $inner_array = array();
       $inner_array["tagName"] = "p";
       $inner_array["type"] = "paragraph";
       $inner_array["label"] = $paragraph->nodeValue;
       $inner_array["value"] = $paragraph->nodeValue;

       $before_id = $this->findBeforeNodeId($paragraph);
       if (isset($before_id)){
         $inner_array["before"] = $before_id;
       }

       $after_id = $this->findAfterNodeId($paragraph);
       if (isset($after_id)){
         $inner_array["after"] = $after_id;
       }

       array_push($paragraph_array, $inner_array);
     }

     return $paragraph_array;
   }

   private function findBeforeNodeId($node){
     $parent = $node->parentNode;
     $parent_previous_sibling = $this->getPreviousSibling($parent, 4);
     $child = $parent_previous_sibling->childNodes[1];

     if (isset($child)){
        return $child->getAttribute("for");

     }
     return null;
   }

   private function findAfterNodeId($node){
     $parent = $node->parentNode;
     $parent_next_sibling = $this->getNextSibling($parent, 4);
     $child = $parent_next_sibling->childNodes[1];

     if (isset($child)){
        return $child->getAttribute("for");

     }
     return null;
   }

   private function getPreviousSibling($node, $times){
     for ($i = 0; $i < $times; $i++){
       if (isset($node->previousSibling))
        $node = $node->previousSibling;
     }
     return $node;
   }

   private function getNextSibling($node, $times){
     for ($i = 0; $i < $times; $i++){
       if (isset($node->nextSibling))
        $node = $node->nextSibling;
     }
     return $node;
   }
 }
