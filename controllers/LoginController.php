<?php
/**
 * Copyright (C) Alfat Saputra Harun - All Rights Reserved
 *
 * @since 1.0
 * @author harunalfat <harunalfat@gmail.com>
 * @copyright Copyright (c) 2016
 * @link http://blog.harunalfat.com/
 */

 namespace app\controllers;

 use Yii;
 use app\modules\user\models\User;
 use app\models\Form;
 use yii\web\Controller;
 use yii\web\Response;
 use yii\web\NotFoundHttpException;
 use yii\helpers\Json;

 class LoginController extends Controller
 {
    public function beforeAction($action){
      $this->enableCsrfValidation = false;
      return parent::beforeAction($action);
    }

    public function actionApi()
    {
      $username = Yii::$app->request->post("username");
      $password = Yii::$app->request->post("password");

      if (!isset($username) || !isset($password)) return "Username/Password Cannot be Empty";

      Yii::$app->response->format = Response::FORMAT_JSON;
      $headers = Yii::$app->response->headers;
      $headers->add('Content-Type', 'application/json');

      $user = User::find()->where(['username' => $username])->one();

      if (isset($user)){
        $valid_password = $user->validatePassword($password);
        if ($valid_password){
          $response["user_id"] = $user->getId();
          $response["access_token"] = $user->getAccessToken();
          return $response;
        }
        else{
          return "Wrong password";
        }
      }
      else{
        return "User not found";
      }
    }
 }
