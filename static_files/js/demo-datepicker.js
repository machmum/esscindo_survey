$( document ).ready(function() {

    $.when(
        $.getScript( "//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" ),
        $.Deferred(function( deferred ){
            $( deferred.resolve );
        })
    ).done(function(){
        if (!Modernizr.inputtypes.date) {
            $('body').css('padding-bottom', '270px'); // Set up min bottom padding for show the datepicker
            $.when(
                $('head').append('<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.css" type="text/css" />'),
                $.getScript( "//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" ),
                $.Deferred(function( deferred ){
                    $( deferred.resolve );
                })
            ).done(function(){
                $('input[type=date]').datepicker({
                    // Consistent format with the HTML5 picker
                    dateFormat: 'yy-mm-dd'
                });
            });
        }
    });

});
