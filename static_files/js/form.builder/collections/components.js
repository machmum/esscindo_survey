/*!
 * Bootstrap 2.3.1 Form Builder
 * Copyright (C) 2012 Adam Moore
 * Licensed under MIT (https://github.com/minikomi/Bootstrap-Form-Builder/blob/gh-pages/LICENSE)
 */

/**
 * Formku
 */

define([
    "jquery" , "underscore" , "backbone"
    , "models/component"
    , "views/tab-component"
    , "views/tab-widget"
], function(
    $, _, Backbone
    , ComponentModel
    , TabComponentView
    , TabWidgetView
    ){
    return Backbone.Collection.extend({
        model: ComponentModel
        , renderAll: function(){
            return this.map(function(component){
                return new TabComponentView({model: component}).render();
            });
        }
        , renderAllAsWidgets: function(){
            return this.map(function(component){
                return new TabWidgetView({model: component}).render();
            });
        }
    });
});