/**
 * Formku
 */

define([
    "jquery", "underscore", "backbone"
    , "models/component"
    , "views/widget", "views/temp-widget"
    , "helper/pubsub"
], function(
    $, _, Backbone
    , ComponentModel
    , WidgetView, TempWidgetView
    , PubSub
    ){
    return WidgetView.extend({
        events:{
            "mousedown" : "mouseDownHandler"
        }
        , mouseDownHandler: function(mouseDownEvent){
            mouseDownEvent.preventDefault();
            mouseDownEvent.stopPropagation();
            //hide all popovers
            $(".popover").hide();
            $("body").append(new TempWidgetView({model: new ComponentModel($.extend(true,{},this.model.attributes))}).render());
            PubSub.trigger("newTempPostRender", mouseDownEvent);
        }
    });
});