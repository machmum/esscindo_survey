/**
 * Formku
 */

define([
    'jquery', 'underscore', 'backbone'
    , "text!templates/app/tab-dropdown.html"

], function($, _, Backbone,
            _tabDropdownTemplate){
    return Backbone.View.extend({
        tagName: "div"
        , className: "tab-pane"
        , initialize: function(options) {
            this.options = options;
            this.id = this.options.title.toLowerCase().replace(/\W/g,'');
            this.tabDropdownTemplate = _.template(_tabDropdownTemplate);
            this.render();
        }
        , render: function(){
            // Render & append nav for tab
            $("#formtabs").append(this.tabDropdownTemplate({title: this.options.title, links: this.options.links, id: this.id}))
        }
    });
});