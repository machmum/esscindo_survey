/*!
 * Bootstrap 2.3.1 Form Builder
 * Copyright (C) 2012 Adam Moore
 * Licensed under MIT (https://github.com/minikomi/Bootstrap-Form-Builder/blob/gh-pages/LICENSE)
 */

/**
 * Formku
 */

define([
    "jquery", "underscore", "backbone"
    , "models/component"
    , "views/component", "views/temp-widget"
    , "helper/pubsub"
], function(
    $, _, Backbone
    , ComponentModel
    , ComponentView, TempWidgetView
    , PubSub
    ){
    return ComponentView.extend({
        events:{
            "mousedown" : "mouseDownHandler"
        }
        , mouseDownHandler: function(mouseDownEvent){
            mouseDownEvent.preventDefault();
            mouseDownEvent.stopPropagation();
            // Hide all popovers
            $(".popover").hide();
            $("body").append(new TempWidgetView({model: new ComponentModel($.extend(true,{},this.model.attributes))}).render());
            PubSub.trigger("newTempPostRender", mouseDownEvent);
        }
    });
});