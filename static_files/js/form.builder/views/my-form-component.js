/*!
 * Bootstrap 2.3.1 Form Builder
 * Copyright (C) 2012 Adam Moore
 * Licensed under MIT (https://github.com/minikomi/Bootstrap-Form-Builder/blob/gh-pages/LICENSE)
 */

/**
 * Formku
 */

define([
    "jquery", "underscore", "backbone", "jquery.tinymce",
    "views/component",
    "helper/pubsub"
], function(
    $, _, Backbone, tinyMCE,
    ComponentView,
    PubSub
    ){
    return ComponentView.extend({
        events:{
            "click"   : "preventPropagation" //stops checkbox / radio reacting.
            , "mousedown" : "mouseDownHandler"
            , "mouseup"   : "mouseUpHandler"
        }

        , mouseDownHandler : function(mouseDownEvent){
            mouseDownEvent.stopPropagation();
            mouseDownEvent.preventDefault();
            var that = this;
            // Popover
            $(".popover").remove();
            this.$el.popover("show");
            $(".popover #save").on("click", this.saveHandler(that));
            $(".popover #cancel").on("click", this.cancelHandler(that));
            // Add drag event for all
                $("body").on("mousemove", function(mouseMoveEvent){
                    if ( Math.abs(mouseDownEvent.pageX - mouseMoveEvent.pageX) > 10 ||
                         Math.abs(mouseDownEvent.pageY - mouseMoveEvent.pageY) > 10 )
                    {
                        that.$el.popover('destroy');
                        PubSub.trigger("myComponentDrag", mouseDownEvent, that.model);
                        that.mouseUpHandler();
                    }
                });
        }

        , preventPropagation: function(e) {
            e.stopPropagation();
            e.preventDefault();
        }

        , mouseUpHandler : function(mouseUpEvent) {
            // Add Wysiwyg editor
            var label = $("#label");
            var text = $("#text");
            var config = {
                skin: "charcoal",
                menubar: false,
                statusbar: false,
                plugins: [
                    'textcolor'
                ],
                toolbar: 'bold, italic, underline, forecolor, backcolor, removeformat',
                force_br_newlines : false,
                force_p_newlines : false,
                forced_root_block : '',
                height: 100
            };

            if (label.tinymce()) {
                label.tinymce().remove();
            }
            if (text.tinymce()) {
                text.tinymce().remove();
            }

            label.tinymce(config);
            text.tinymce(config);

            $("body").off("mousemove");
        }

        , saveHandler : function(boundContext) {
            return function(mouseEvent) {
                mouseEvent.preventDefault();
                var fields = $(".popover .field");
                _.each(fields, function(e){

                    var $e = $(e)
                        , type = $e.attr("data-type")
                        , name = $e.attr("id");

                    switch(type) {
                        case "checkbox":
                            boundContext.model.setField(name, $e.is(":checked"));
                            break;
                        case "input":
                            boundContext.model.setField(name, $e.val());
                            break;
                        case "textarea":
                            boundContext.model.setField(name, $e.val());
                            break;
                        case "textarea-split":
                            boundContext.model.setField(name,
                                _.chain($e.val().split("\n"))
                                    .map(function(t){return $.trim(t)})
                                    .filter(function(t){return t.length > 0})
                                    .value()
                            );
                            break;
                        case "select":
                            var valarr = _.map($e.find("option"), function(e){
                                return {value: e.value, selected: e.selected, label:$(e).text()};
                            });
                            boundContext.model.setField(name, valarr);
                            break;
                    }
                });
                boundContext.model.trigger("change");
                $(".popover").remove();
            }
        }

        , cancelHandler : function(boundContext) {
            return function(mouseEvent) {
                mouseEvent.preventDefault();
                $(".popover").remove();
                boundContext.model.trigger("change");
            }
        }

    });
});