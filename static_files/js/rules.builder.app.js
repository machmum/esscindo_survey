/**
 * Copyright (C) Baluart.COM - All Rights Reserved
 *
 * @since 1.0
 * @author Balu
 * @copyright Copyright (c) 2015 - 2016 Baluart.COM
 * @license http://codecanyon.net/licenses/faq Envato marketplace licenses
 * @link http://easyforms.baluart.com/ Easy Forms
 */

// Patch Model and Collection so they emit a 'fetch' event when starting to fetch data
_.each(["Model", "Collection"], function(name) {
    // Cache Backbone constructor.
    var ctor = Backbone[name];
    // Cache original fetch.
    var fetch = ctor.prototype.fetch;

    // Override the fetch method to emit a fetch event.
    ctor.prototype.fetch = function() {
        // Trigger the fetch event on the instance.
        this.trigger("fetch", this);

        // Pass through to original fetch.
        return fetch.apply(this, arguments);
    };
});

// Patch View, add a close method
Backbone.View.prototype.close = function(){
    this.remove();
    this.unbind();
    if (this.onClose){
        this.onClose();
    }
};

//******************
// Underscore
//******************

// Override template settings
_.templateSettings = {
    evaluate: /\{\{(.+?)\}\}/g,
    interpolate: /\{\{=(.+?)\}\}/g,
    escape: /\{\{-(.+?)\}\}/g
};

//******************
// App
//******************

var App = App || {};

App.Data = {
    "variables": options.variables,
    "actions": [
        {
            "name": "toShow",
            "label": options.i18n.show,
            "fields": [{"label": "", "name": "target", "fieldType": "select", "options": [
                {"label": options.i18n.field, "name": "field", "fields": [
                    {"label": "", "name": "targetField", "fieldType": "select", "options": options.fields}
                ]},
                {"label": options.i18n.element, "name": "element", "fields": [
                    {"label": "", "name": "targetElement", "fieldType": "text"}
                ]}
            ]}]
        },
        {
            "name": "toHide",
            "label": options.i18n.hide,
            "fields": [{"label": "", "name": "target", "fieldType": "select", "options": [
                {"label": options.i18n.field, "name": "field", "fields": [
                    {"label": "", "name": "targetField", "fieldType": "select", "options": options.fields}
                ]},
                {"label": options.i18n.element, "name": "element", "fields": [
                    {"label": "", "name": "targetElement", "fieldType": "text"}
                ]}
            ]}]
        },
        {
            "name": "toEnable",
            "label": options.i18n.enable,
            "fields": [{
                "name": "field",
                "label": options.i18n.field,
                "fieldType": "select",
                "options": options.fields
            }]
        },
        {
            "name": "toDisable",
            "label": options.i18n.disable,
            "fields": [{
                "name": "field",
                "label": options.i18n.field,
                "fieldType": "select",
                "options": options.fields
            }]
        },
        {label: options.i18n.copy, name: "copy", fields: [
            {"label": options.i18n.from, "name": "original", "fieldType": "select", "options": [
                {"label": options.i18n.field, "name": "field", "fields": [
                    {"label": "", "name": "originalField", "fieldType": "select", "options": options.fields}
                ]},
                {"label": options.i18n.element, "name": "element", "fields": [
                    {"label": "", "name": "originalElement", "fieldType": "text"}
                ]}
            ]},
            {"label": options.i18n.to, "name": "target", "fieldType": "select", "options": [
                {"label": options.i18n.field, "name": "field", "fields": [
                    {"label": "", "name": "targetField", "fieldType": "select", "options": options.fields}
                ]},
                {"label": options.i18n.element, "name": "element", "fields": [
                    {"label": "", "name": "targetElement", "fieldType": "text"}
                ]}
            ]}
        ]},
        {label: options.i18n.math, name: "performArithmeticOperations", fields: [
            {"label": options.i18n.perform, "name": "operator", "fieldType": "select", "options": [
                {label: options.i18n.addition, name: "+"},
                {label: options.i18n.subtraction, name: "-"},
                {label: options.i18n.multiplication, name: "*"},
                {label: options.i18n.division, name: "/"},
                {label: options.i18n.remainder, name: "%"}
            ]},
            {"label": options.i18n.of, "name": "operands", "fieldType": "select_multiple", "options": options.fields},
            {"label": options.i18n.andSetResultTo, "name": "target", "fieldType": "select", "options": [
                {"label": options.i18n.field, "name": "field", "fields": [
                    {"label": "", "name": "targetField", "fieldType": "select", "options": options.fields}
                ]},
                {"label": options.i18n.element, "name": "element", "fields": [
                    {"label": "", "name": "targetElement", "fieldType": "text"}
                ]}
            ]}
        ]},
        {
            "name": "formatNumber",
            "label": options.i18n.formatNumber,
            "fields": [{"label": options.i18n.of, "name": "target", "fieldType": "select", "options": [
                {"label": options.i18n.field, "name": "field", "fields": [
                    {"label": "", "name": "targetField", "fieldType": "select", "options": options.fields}
                ]},
                {"label": options.i18n.element, "name": "element", "fields": [
                    {"label": "", "name": "targetElement", "fieldType": "text"}
                ]}
            ]}, {"label": "As", "name": "format", "fieldType": "text"}
            ]
        },
        {
            "name": "skip",
            "label": options.i18n.skip,
            "fields": [{
                "name": "step",
                "label": options.i18n.toStep,
                "fieldType": "select",
                "options": options.steps
            }]
        }
    ],
    "variable_type_operators": {

        // Text component
        "text": textOperators,
        "tel": textOperators,
        "color": colorOperators,
        "url": textOperators,
        "password": textOperators,

        // Number component
        "number": numberOperators,
        "range": numberOperators,

        // Date component
        "date": dateOperators,
        "datetime-local": dateOperators,
        "time": dateOperators,
        "month": dateOperators,
        "week": dateOperators,

        // Email component
        "email": emailOperators,

        // TextArea component
        "textarea": textOperators,

        // Select List component
        "select": selectOperators,

        // Checkbox component
        "checkbox": checkboxOperators,

        // Radio component
        "radio": radioOperators,

        // Hidden component
        "hidden": hiddenOperators,

        // File component
        "file": fileOperators,

        // Form
        "form": formOperators
    }
};

//******************
// App Views
//******************

App.RulesView = Backbone.View.extend({
    className: 'rule-builder',
    subViews: {},
    template: _.template($('#rules-template').html()),
    events: {
        'click button#add-rule': function(event) {
            event.preventDefault();
            this.collection.add(
                {
                    form_id: options.formID,
                    status: 1
                }
            );
        }
    },
    initialize: function(){
        // Event Listeners
        this.listenTo(this.collection, 'add', this.onAdd);
        this.listenTo(this.collection, 'destroy', this.onDestroy);
    },
    onAdd: function(model){
        this.addSubview(model);
    },
    onDestroy: function(model){
        this.subViews[model.cid].close();
    },
    addSubview: function( model ) {
        // Add rule view
        this.subViews[model.cid] = new App.RuleView({ model: model });
        this.$('#rules').append(this.subViews[model.cid].render().el);
    },
    closeSubviews: function() {
        // Call destroy method for each view
        _.invoke(this.subViews, 'close');
    },
    onClose: function() {
        // Destroy subViews
        this.closeSubviews();
        // Destroy this view
        this.undelegateEvents();
        this.$el.removeData().unbind();
        // Remove view from DOM
        this.remove();
        Backbone.View.prototype.remove.call(this);
    },
    render: function(){
        // clean views before rendering new ones
        this.closeSubviews();
        this.el.innerHTML = this.template();
        this.collection.each(this.addSubview.bind(this));
        return this;
    }
});

App.RuleView = Backbone.View.extend({
    template: _.template($('#rule-template').html()),
    tagName: 'div',
    className: 'rules-group-container',
    events: {
        'click form .btn': 'changeModel',
        'remove .remove': 'changeModel',
        'change :input': 'changeModel',
        'click .save-rule': 'saveModel',
        'click .delete-rule': 'deleteModel'
    },
    initialize: function(){
        // Event Listeners
        this.listenTo(this.model, 'change', this.onChange);
    },
    onChange: function(){
        // Update the view
        this.render();
    },
    changeModel: function(event) {
        event.preventDefault();
        // Show unsaved changes message
        this.$(".label-warning").show();
    },
    saveModel: function(event){
        event.preventDefault();
        // Save on DB
        var conditions = this.$("#" + this.model.cid + "conditions");
        var actions = this.$("#" + this.model.cid + "actions");
        var status = this.$("#" + this.model.cid + "status").is(':checked') ? 1 : 0;
        console.log();
        this.model.set({
            'conditions': JSON.stringify(conditions.conditionsBuilder('data')),
            'actions': JSON.stringify(actions.actionsBuilder('data')),
            'status': status
        });
        this.model.save();
    },
    deleteModel: function(event){
        event.preventDefault();
        if( this.model.isNew() ) {
            this.model.destroy();
        } else {
            // Delete on DB
            if(confirm(options.i18n.areYouSureDeleteItem)) {
                this.model.destroy();
            }
        }
        return false;
    },
    onClose: function() {
        // Destroy this view
        this.undelegateEvents();
        this.$el.removeData().unbind();
        // Remove view from DOM
        this.remove();
        Backbone.View.prototype.remove.call(this);
    },
    displayRuleBuilder: function () {
        // Initialize Rule Builder
        var conditions = this.$("#" + this.model.cid + "conditions");
        var actions = this.$("#" + this.model.cid + "actions");

        // Update conditions data and pass to builder
        if(this.model.has('conditions') && _.isString(this.model.get("conditions"))) {
            conditions.conditionsBuilder($.extend( {}, App.Data, {
                data: JSON.parse(this.model.get("conditions"))
            } ));
        } else {
            conditions.conditionsBuilder(App.Data);
        }

        // Update actions data and pass to builder
        if(this.model.has('actions') && _.isString(this.model.get("actions"))) {
            actions.actionsBuilder($.extend( {}, App.Data, {
                data: JSON.parse(this.model.get("actions"))
            } ));
        } else {
            actions.actionsBuilder(App.Data);
        }
    },
    render: function(){

        this.el.innerHTML = this.template({
            cid: this.model.cid,
            rule: this.model.toJSON()
        });

        // Display unsaved changes warning
        if(this.model.isNew()) {
            this.$(".label-warning").show();
        }

        // Display rule builder
        this.displayRuleBuilder();

        return this;
    }
});

//******************
// App Router
//******************

var Router = Backbone.Router.extend({
    views:{},
    initialize: function(options){
        this.main = options.main;
        this.rules = options.rules;
    },
    routes: {
        '': 'index'
    },
    closeViews: function() {
        // Call close method for each view
        _.invoke(this.views, 'close');
    },
    index: function(){
        this.closeViews();
        this.views['rulesView'] = new App.RulesView({ collection: this.rules });
        this.main.append(this.views['rulesView'].render().el);
    }
});

//******************
// App Collections
//******************

var Rule = Backbone.Model.extend({
    url: function() {
        var base = _.result(this, 'urlRoot') || _.result(this.collection, 'url') || urlError();
        if (this.isNew()) return base;
        return base + (base.charAt(base.length - 1) === '/' ? '' : '&id=') + encodeURIComponent(this.id);
    },
    methodUrl: function(method){
        if(method == "delete"){
            return options.deleteEndPoint + "&id=" + this.attributes.id;
        }else if(method == "update"){
            return options.updateEndPoint + "&id=" + this.attributes.id;
        }else if(method == "create"){
            return options.createEndPoint + "&id=" + this.attributes.id;
        }
        return false;
    },
    sync: function(method, model, options) {
        if (model.methodUrl && model.methodUrl(method.toLowerCase())) {
            options = options || {};
            options.url = model.methodUrl(method.toLowerCase());
        }
        Backbone.sync(method, model, options);
    },
    initialize: function(){
    }
});

var Rules = Backbone.Collection.extend({
    url: options.endPoint,
    model: Rule,
    initialize: function() {
        if (options.hasPrettyUrls) {
            this.model = Backbone.Model;
        }
    },
    parse: function(resp) {
        this.pager = resp._meta;
        return resp.items;
    },
    fetchPage: function() {
        var self = this;
        return this.fetch({
            data: $.param({ id: options.formID }),
            reset: true,
            success:function(){
                self.trigger("sync:page")
            }
        });
    }
});

//******************
// App Init
//******************

App.init = function(){

    // Server Data
    App.Rules = new Rules();
    return App.Rules.fetchPage().then(function(){
        App.Router = new Router({
            main: $("#main"),
            rules: App.Rules
        });
        Backbone.history.start();
    });

};

//******************
// App run
//******************

$(function() {

    App.init();

});

