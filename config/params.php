<?php

return [
    // Mailer
    'App.Mailer.transport' => 'smtp', // 'smtp' or 'php'
    // GridView
    'GridView.pagination.pageSize' => 5,
    // List Group
    'ListGroup.listSize' => 5,
    // Google Maps API Key
    // https://developers.google.com/maps/documentation/javascript/get-api-key
    'Google.Maps.apiKey' => '',
    // Cron Jobs
    'App.Mailer.cronExpression' => '* * * * *', // Process mail queue every minute
    'App.Analytics.cronExpression' => '@daily', // Update analytics every day
    // Overwrite PHP Path
    'App.Console.phpPath' => '', // Absolute path to php. Eg. '/usr/bin/php'
];