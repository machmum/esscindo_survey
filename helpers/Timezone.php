<?php
/**
 * Copyright (C) Baluart.COM - All Rights Reserved
 *
 * @since 1.0
 * @author Balu
 * @copyright Copyright (c) 2015 - 2016 Baluart.COM
 * @license http://codecanyon.net/licenses/faq Envato marketplace licenses
 * @link http://easyforms.baluart.com/ Easy Forms
 */

namespace app\helpers;

use Yii;

/**
 * Class Timezone
 * @package app\helpers
 */
class Timezone
{
    const SORT_NAME   = 0;
    const SORT_OFFSET = 1;
    public static $template = '{name} {offset}';
    public static $sortBy = 0;

    public static function all()
    {
        $timeZones = [];
        $timeZonesOutput = [];
        $now = new \DateTime('now', new \DateTimeZone('UTC'));
        foreach (\DateTimeZone::listIdentifiers(\DateTimeZone::ALL) as $timeZone) {
            $now->setTimezone(new \DateTimeZone($timeZone));
            $timeZones[] = [$now->format('P'), $timeZone];
        }
        if (self::$sortBy == static::SORT_OFFSET) {
            array_multisort($timeZones);
        }

        foreach ($timeZones as $timeZone) {
            $content = preg_replace_callback("/{\\w+}/", function ($matches) use ($timeZone) {
                switch ($matches[0]) {
                    case '{name}':
                        return $timeZone[1];
                    case '{offset}':
                        return $timeZone[0];
                    default:
                        return $matches[0];
                }
            }, self::$template);
            $timeZonesOutput[$timeZone[1]] = $content;
        }

        return $timeZonesOutput;
    }

    public function convertMS($ms) {

        $timeShow = "00 : 00 : 00";
        if ($ms > 0) {
            $second     = floor($ms / 1000);
            $minutes    = floor($second / 60);

            $second     = $second % 60;
            $second     = $second > 9 ? $second : '0' . $second;
            $hour       = floor($minutes / 60);

            $minutes    = $minutes % 60;
            $minutes    = $minutes > 9 ? $minutes : '0' . $minutes;

            $hour       = $hour % 24;
            $hour       = $hour > 9 ? $hour : '0' . $hour;

            if ($hour > 0) {
                $timeTitle = $hour . ' hour ' . $minutes . ' minutes ' . $second . ' second';
            } else {
                if ($minutes > 0) {
                    $timeTitle = $minutes . ' minutes ' . $second . ' second';
                } else {
                    $timeTitle = $second . ' second';
                }
            }
            $timeShow = $hour . ' : ' . $minutes . ' : ' . $second;
        }

        return $timeShow;
    }

    public function convertTimeLocal($ms) {

        $result = "Data not found";
        if ($ms > 0) {
            $sec = floor($ms / 1000);

            $result = date("m/d/Y H:i:s", $sec);
        }

        return $result;
    }

}
