# Changelog


30.01.2016 - ver 1.3.1

- Added: Console component
- Deprecated: ConsoleHelper
- Improved: Setup / Update modules
- Improved: Cron
- Improved: Tel field validation message
- Fixed: Dashboard for advanced users
- Fixed: Performance tool
- Fixed: Required fields without labels
- Fixed: Actions buttons for advanced users
- Fixed: Google Analytics add-on event handler


25.01.2016 - ver 1.3

- Added: User registration page
- Added: Login page without password
- Added: Enable / Disable user registration from Site Settings
- Added: Add captcha to user registration from Site Settings
- Added: Set a default user rol from Site Settings
- Added: New user role: 'Advanced User' and 'User' now is 'Basic User'
- Added: Display Form Manager 'Actions' button to all users
- Added: Refresh cache tool
- Added: HTTP and HTTPS protocols supported
- Added: Print form submission
- Added: Format Number action on Form Rules
- Added: Indonesian translation
- Improved: Grid Views footer design
- Improved: Check user permissions
- Improved: Change login page when 'anyone can register' is enabled
- Improved: Addons module can update each addon version
- Improved: Install Process, update and uninstall addons
- Improved: Addons module events
- Improved: User module updated to new version
- Improved: Upload File validation on Multi Step forms
- Improved: Multi-Step forms pager
- Improved: Button component now supports 'button' input type
- Improved: Form Builder. Add images or icons to checkboxes or radio buttons
- Improved: Form Builder. Add icons to buttons
- Improved: Form Builder. New button input type: 'button'
- Fixed: Setup module error message
- Fixed: Export CSV file with 'file' fields
- Fixed: Reset password email
- Fixed: Delete multiple themes
- Fixed: Delete multiple templates
- Fixed: Default local IP for testing
- Fixed: XSS vulnerability on Submission Manager


14.01.2016 - ver 1.2

- Added: Pre-fill Form Widget with default values
- Added: Password Protected Forms
- Added: Filters in Form Manager, Templates and Themes
- Added: Rules Engine. If the condition is not met, the skip to step will be reset
- Improved: Form Builder with duplicated fields detector
- Improved: Form Settings design
- Improved: New migrations
- Improved: Spanish language translation
- Improved: Vendors updated
- Improved: Submission Event Handler with multiple cc and multiple bcc
- Fixed: Email notifications with array value
- Fixed: Resize Form Widget on IE
- Fixed: ThemeSearch table prefix
- Fixed: UserSearch profile attribute
- Fixed: Validate the field type only if input has a value
- Fixed: Form Builder without mod_rewrite
- Fixed: Delete Multiple Action on Form Manager
- Fixed: Form Builder's checkbox and radio button edition on Firefox
- Fixed: Form Builder's checkbox and radio button edition on Firefox
- Fixed: Dashboard Labels on Firefox


07.01.2016 - ver 1.1

- Added: Webhooks Add-on
- Added: Forms with Friendly Urls
- Added: Update Module
- Added: Now Easy Forms works without mod_rewrite
- Added: 'record' param to Form Widget
- Added: Cron jobs configuration with params
- Added: Email delivery with PHP mail() function
- Improved: Glyphicons version 1.9.2
- Improved: Run migrations on background
- Improved: Redirection and messaging after form submission
- Improved: In-App Analytics configurations
- Improved: Dashboard dates
- Improved: Add icon to Rule Builder error message
- Improved: Add icon to Report Builder success message
- Improved: Spanish translation
- Improved: Button custom text on multi-step forms
- Fixed: Show / hide columns on Submission Manager
- Fixed: Form Manager with table prefix
- Fixed: Submission event handler
- Fixed: Log out link
- Fixed: Export submissions as CSV file

29.12.2015 - Initial release