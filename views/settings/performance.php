<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;

$this->title = Yii::t('app', 'Performance');

$this->params['breadcrumbs'][] = ['label' => $this->title];

?>
    <div class="account-management">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="glyphicon glyphicon-settings" style="margin-right: 5px;"></i>
                    <?= Html::encode($this->title) ?>
                </h3>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin(); ?>
                    <div class="col-sm-12" style="padding: 0;">
                        <?= Html::tag('h4', Yii::t('app', 'Refresh cache & assets'), [
                            'style' => 'font-size: 14px; font-weight: bold; padding: 0; margin: 0;',
                        ]) ?>
                        <div class="form-group" style="margin-top: 10px; margin-bottom: 0">
                            <?= Html::submitButton(Html::tag('i', ' ', [
                                    'class' => 'glyphicon glyphicon-refresh'
                                ]) . ' ' . Yii::t('app', 'Refresh'), ['class' => 'btn btn-primary']) ?>
                        </div>
                    </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    </div>