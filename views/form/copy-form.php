<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon_32.png" sizes="32x32">
    <link rel="icon" href="/favicon_48.png" sizes="48x48">
    <link rel="icon" href="/favicon_96.png" sizes="96x96">
    <link rel="icon" href="/favicon_144.png" sizes="144x144">
    <meta name="csrf-param" content="_csrf">
    <meta name="csrf-token" content="TGx4dU1sN0kHWzUveUEBGitcSRguFQMtJBQIFCQqe3E2WS0EDBYFPA==">
    <title>Copy Form | Iona AcceleratIFF</title>
    <link href="/static_files/css/fonts.min.css" rel="stylesheet">
<link href="/static_files/css/bootstrap.min.css" rel="stylesheet">
<link href="/assets/360be1da/css/kv-detail-view.min.css" rel="stylesheet">
<link href="/assets/f98c9be6/css/bootstrap-switch.min.css" rel="stylesheet">
<link href="/assets/f98c9be6/css/bootstrap-switch-kv.min.css" rel="stylesheet">
<link href="/static_files/css/app.min.css" rel="stylesheet">
<script type="text/javascript">var kvDetailView_51f1e6b6 = {"fadeDelay":800,"alertTemplate":"\u003Cdiv class=\u0022{class} fade in\u0022\u003E\u003Cbutton type=\u0022button\u0022 class=\u0022close\u0022 data-dismiss=\u0022alert\u0022 aria-hidden=\u0022true\u0022\u003E\u0026times;\u003C\/button\u003E{content}\u003C\/div\u003E","alertMessageSettings":{"kv-detail-error":"alert alert-danger","kv-detail-success":"alert alert-success","kv-detail-info":"alert alert-info","kv-detail-warning":"alert alert-warning"},"deleteParams":[],"deleteAjaxSettings":[],"deleteConfirm":"Are you sure you want to delete this item?","showErrorStack":false};
</script>
</head>
<body class="main form form-view">

<div class="wrap">

    
        <nav id="w1" class="navbar-inverse navbar-fixed-top navbar" role="navigation"><div class="container"><div class="navbar-header"><button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#w1-collapse"><span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span></button><a class="navbar-brand" href="/" title="Online Survey Apps of PT Essence Indonesia (IFF)"><span class="app-name">Iona AcceleratIFF</span></a></div><div id="w1-collapse" class="collapse navbar-collapse">
        <ul id="w2" class="navbar-nav navbar-right nav"><li><a href="/dashboard">Dashboard</a></li>
<li class="active"><a href="/form">Forms</a></li>
<li><a href="/theme">Themes</a></li>
<li><a href="/addons">Uploads</a></li>
<li><a href="/user/admin">Users</a></li>
<li class="dropdown hasAvatar"><a class="dropdown-toggle" href="/user" data-toggle="dropdown"><img class="avatar" src="/static_files/images/avatars/placeholder.png" alt=""> vt_hanif <b class="caret"></b></a><ul id="w3" class="dropdown-menu"><li><a href="/user/profile" tabindex="-1">Manage account</a></li>
<li><a href="/settings/site" tabindex="-1">Settings</a></li>
<li class="divider"></li>
<li><a class="highlighted" href="/user/logout" data-method="post" tabindex="-1">Logout</a></li></ul></li></ul>
        </div></div></nav>
        <div class="container">
            <ul class="breadcrumb breadcrumb-arrow"><li><a href="/dashboard">Dashboard</a></li>
<li><a href="/form/index">Forms</a></li>
<li class='active'><span>Copy Form</span></li>
</ul>                        <div class="form-view box box-big box-light">

    <div class="box-header">
        <h3 class="box-title"><span class="box-subtitle">Copy Form</span>
        </h3>
    </div>

<div id="w0"><div class="table-responsive kv-detail-view"><table class="kv-view-mode table table-hover table-bordered table-striped detail-view" data-krajee-kvDetailView="kvDetailView_51f1e6b6"><tr class="info"><th colspan="2">Choose Form that You Want to Copy</th></tr>

                       
                                    
<tr><th style="width: 20%; text-align: right; vertical-align: middle;">Form Name</th>
<td><div class="kv-attribute">
<form action="copy-form-check.php" method="post">
<?php
 $servername = "localhost";
                                    $username = "velotech_user";
                                    $password = "muxuptol";
                                    $dbname = "velotech_essc_indo";

                                    // Create connection
                                    $conn = new mysqli($servername, $username, $password, $dbname);
                                    // Check connection
                                    if ($conn->connect_error) {
                                            die("Connection failed: " . $conn->connect_error);
                                    } 


                                        $sql = "SELECT * FROM form";
                                        $result = $conn->query($sql);
                                        if ($result->num_rows > 0) {
                                            // output data of each row
                                            echo "<select class='form-control' name='users'>";
                                            echo "<option value=''>Select a Form:</option>";
                                            while($row = $result->fetch_assoc()) {
											$title = strip_tags($row['name']);												
                                            echo "<option title='" . $title . "'value='" . $row['id'] . "'>" . $row['name'] . "</option>";
                                            }
                                            echo "</select>";
                                        } else {
                                            echo "0 results";
                                        }						
                                    ?>

</div>
</td></tr>

<!--- <tr><th style="width: 20%; text-align: right; vertical-align: middle;">Form ID</th>
<td><div class="kv-attribute">99</div>
</td></tr> --->


<tr><th style="width: 20%; text-align: right; vertical-align: middle;"></th>
<td>

<input class="btn btn-primary" type="submit" name="Submit" value="Copy Form">

</td></tr>
</form>
</table><p><b>NOTE : </b></p>
		<p>(1) Copy-an Form Berupa Struktur (Field Pertanyaan), untuk Rule tidak bisa di-copy!</p>
		<p>(2) Format hasil copy questioner : <b>Copy - [nama form yg dicopy]</b></p>
</div></div>
</div>
        </div>

        <footer class="footer">
            <div class="container">
                <p class="pull-right">&copy; Iona AcceleratIFF 2016</p>
            </div>
        </footer>

    </div>

<script src="/static_files/js/libs/jquery.js"></script>
<script src="/assets/e28167df/yii.js"></script>
<script src="/assets/360be1da/js/kv-detail-view.min.js"></script>
<script src="/static_files/js/libs/bootstrap.min.js"></script>
<script src="/assets/f98c9be6/js/bootstrap-switch.min.js"></script>
<script type="text/javascript">jQuery(document).ready(function () {

jQuery("#w0").kvDetailView(kvDetailView_51f1e6b6);

jQuery("#w0").find("[data-toggle=tooltip]").tooltip();
});</script></body>
</html>
