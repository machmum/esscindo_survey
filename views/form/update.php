<?php

use yii\web\View;
use yii\helpers\Url;
use app\bundles\FormBuilderBundle;

/* @var $this yii\web\View */
/* @var $model app\models\Form */

FormBuilderBundle::register($this);

// PHP options required by main-built.js
$options = array(
    "libUrl" => Url::to('@web/static_files/js/form.builder/lib/'),
    "i18nUrl" => Url::to(['ajax/builder-phrases']),
    "componentsUrl" => Url::to(['ajax/builder-components']),
    "initPoint" => Url::to(['ajax/init-form', 'id' => $model->id]),
    "endPoint" => Url::to(['ajax/update-form', 'id' => $model->id]),
    "reCaptchaSiteKey" => Yii::$app->settings->get("app.reCaptchaSiteKey"),
    "afterSave" => 'showMessage', // Or 'redirect'
    "redirectTo" => Url::to(['/form']),
    "_csrf" => Yii::$app->request->getCsrfToken(),
);

// Pass php options to javascript
$this->registerJs("var options = ".json_encode($options).";", View::POS_BEGIN, 'builder-options');

$this->title = Yii::t('app', 'Update Form');
?>
<script>
function showUser(str) {
    if (str == "") {
        document.getElementById("txtHint").innerHTML = "<b>Choose your saved question</b>";
        return;
    }
    else { 	
        var http = new XMLHttpRequest();
        var url = "https://www.esscindo.com/models/GetQuest.php";
        var params = "q="+str;
        http.open("POST", url, true);

        //Send the proper header information along with the request
        http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        http.setRequestHeader("Content-length", params.length);
        http.setRequestHeader("Connection", "close");

        http.onreadystatechange = function() {//Call a function when the state changes.
            if(http.readyState == 4 && http.status == 200) {
                document.getElementById("txtHint").innerHTML = http.responseText;         
            }
        };
        http.send(params);
        
    };
}

function deleteQuestion(id) {
    var x = confirm('Delete this question?');
    if(x === true){
        var http = new XMLHttpRequest();
        var url = "https://www.esscindo.com/models/DelQuest.php";

        http.open("POST", url, true);
        var params = "q="+id;
        //Send the proper header information along with the request
        http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        http.setRequestHeader("Content-length", params.length);
        http.setRequestHeader("Connection", "close");

        http.onreadystatechange = function() {//Call a function when the state changes.
            if(http.readyState == 4 && http.status == 200) {
		
               //window.alert('Delete Succesfully');   
	       window.location.reload(true);
   
            };
        };
        http.send(params);


        
    }else{
        return false;
    };
};
</script>
<div class="form-update">
    <div class="row">

        <!-- Widgets -->
        <div class="col-md-4 sidebar-outer">
            <div class="sidebar">
                <div class="panel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs nav-justified" id="formtabs" role="tablist">
                        <!-- Tab nav -->
						<li>
							<a href="#questionlib" aria-controls="questionlib" role="tab" data-toggle="tab" aria-expanded="false">QuestLib</a>
						</li>
                    </ul>
                    <form id="widgets">
                        <fieldset>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <!-- Tabs of widgets go here -->
								<div id="questionlib" class="tab-pane" role="tabpanel">
                                    <?php 
                                    $servername = "localhost";
                                    $username = "velotech_user";
                                    $password = "muxuptol";
                                    $dbname = "velotech_essc_indo";

                                    // Create connection
                                    $conn = new mysqli($servername, $username, $password, $dbname);
                                    // Check connection
                                    if ($conn->connect_error) {
                                            die("Connection failed: " . $conn->connect_error);
                                    } 

                                    ?>
                                    <label>Saved Questions</label>
                                    <pre class=" language-markup" style="overflow-y: scroll; height:400px;">
                                    <?php
                                        $sql = "SELECT * FROM save_to_lib";
                                        $result = $conn->query($sql);
                                        if ($result->num_rows > 0) {
                                            // output data of each row
                                            echo "<select class='form-control' name='users' onchange='showUser(this.value)'>";
                                            echo "<option value=''>Select a Question:</option>";
                                            while($row = $result->fetch_assoc()) {
											$title = strip_tags($row['label']);												
                                            echo "<option title='" . $title . "'value='" . $row['id'] . "'>" . substr($row['label'],0,37) . "</option>";
                                            }
                                            echo "</select>";
                                        } else {
                                            echo "0 results";
                                        }						
                                    ?>
                                    <div id="txtHint"><b>Choose your saved question</b></div>
                                    </pre>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <!-- / Widgets -->

        <!-- Building Form. -->
        <div class="col-md-8">
            <!-- Alert. -->
            <div class="alert alert-warning alert-dismissable fade" style="display: none">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong><?= Yii::t("app", "Tip") ?>:</strong>
                <?= Yii::t(
                    "app",
                    "Just Drag the Fields on center to start building your form. It's fast, easy & fun."
                ) ?>
            </div>
            <!-- / Alert. -->
            <div id="canvas">
                <form id="my-form">
                </form>
            </div>
            <div id="messages">
                <div data-alerts="alerts"
                     data-titles="{'warning': '<em><?= Yii::t("app", "Warning!") ?></em>'}"
                     data-ids="myid" data-fade="2000"></div>
            </div>
            <div id="actions">
                <input id="formId" type="hidden" value="<?= $model->id ?>">
                <button type="button" class="btn btn-default" id="saveForm">
                    <span class="glyphicon glyphicon-ok"></span> <?= Yii::t("app", "Save Form") ?></button>
            </div>
        </div>
        <!-- / Building Form. -->

        <!-- .modal -->
        <div class="modal fade" id="saved">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                                title="<?= Yii::t('app', 'I still want to edit this form.') ?>">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?= Yii::t("app", "Great! Your form is saved.") ?></h4>
                    </div>
                    <div class="modal-body">
                        <p><?= Yii::t("app", "What do you want to do now?") ?></p>
                        <div class="list-group">
                            <a href="<?= $url = Url::to(['form/update', 'id' => $model->id]); ?>" id="toUpdate"
                               class="list-group-item">
                                <h4 class="list-group-item-heading"><?= Yii::t("app", "It's Ok.") ?></h4>
                                <p class="list-group-item-text">
                                    <?= Yii::t("app", "I still want to edit this form.") ?></p></a>
                            <a href="<?= $url = Url::to(['form/settings', 'id' => $model->id]); ?>" id="toSettings"
                               class="list-group-item">
                                <h4 class="list-group-item-heading">
                                    <?= Yii::t("app", "Let’s go to Form Settings.") ?></h4>
                                <p class="list-group-item-text">
                                    <?= Yii::t("app", "I need to setup my email notifications, confirmation options and more.") ?></p></a>
                            <a href="<?= $url = Url::to(['form/index']); ?>" class="list-group-item">
                                <h4 class="list-group-item-heading">
                                    <?= Yii::t("app", "I finished! Take me back to the Form Manager.") ?></h4>
                                <p class="list-group-item-text">
                                    <?= Yii::t("app", "I want to publish and share my form.") ?></p></a>
                        </div>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

    </div>
</div>