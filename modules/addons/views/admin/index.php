<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use app\components\widgets\ActionBar;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\addons\models\AddonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('addon', 'Uploads');
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    [
        'class' => '\kartik\grid\CheckboxColumn',
        'headerOptions' => ['class'=>'kartik-sheet-style'],
        'rowSelectedClass' => GridView::TYPE_WARNING,
    ],
    [
        'attribute'=> 'name',
        'format' => 'raw',
        'value' => function ($model) {
            if ($model->installed && $model->status) {
                return Html::a(Html::encode($model->name), ['/addons/' . $model->id]);
            }
            return $model->name;
        },
    ],
    [
        'attribute'=>'version',
        'value'=> 'version',
    ],
    [
        'class'=>'kartik\grid\BooleanColumn',
        'attribute'=>'installed',
        'trueIcon'=>'<span class="glyphicon glyphicon-ok text-success"></span>',
        'falseIcon'=>'<span class="glyphicon glyphicon-remove text-danger"></span>',
        'vAlign'=>'middle',
    ],
    [
        'class'=>'kartik\grid\BooleanColumn',
        'attribute'=>'status',
        'trueIcon'=>'<span class="glyphicon glyphicon-ok text-success"></span>',
        'falseIcon'=>'<span class="glyphicon glyphicon-remove text-danger"></span>',
        'vAlign'=>'middle',
    ],
    [
        'attribute'=>'description',
        'value'=> 'description',
    ],
];

?>
<div class="addons-index">
    <div class="row">
        <div class="col-md-12">

<div id="addons-grid" class="grid-view" data-krajee-grid="kvGridInit_6027987a"><div class="panel panel-info">
    <div class="panel-heading">    <div class="pull-right">
        
    </div>
    <h3 class="panel-title">
        Uploads <small class="panel-subtitle hidden-xs">Upload your files here</small>
    </h3>
    <div class="clearfix"></div></div>
    <div class="kv-panel-before">    <div class="pull-right">
        <div class="btn-toolbar kv-grid-toolbar" role="toolbar">
        </div>    
    </div>
<iframe src="https://esscindo.com/uploads/" style="width:100%;min-height: 400px; overflow-x:hidden;overflow-y:scroll;border:0;"></iframe>
    <div class="clearfix"></div>
    </div>
    <div class="kv-panel-after"></div>
</div></div>
        </div>
    </div>
</div>
<?php
$js = <<< 'SCRIPT'

$(function () {
    $("[data-toggle='tooltip']").tooltip();
});;

SCRIPT;
// Register tooltip/popover initialization javascript
$this->registerJs($js);