<?php
/**
 * Copyright (C) Baluart.COM - All Rights Reserved
 *
 * @since 1.1
 * @author Balu
 * @copyright Copyright (c) 2015 - 2016 Baluart.COM
 * @license http://codecanyon.net/licenses/faq Envato marketplace licenses
 * @link http://easyforms.baluart.com/ Easy Forms
 */

namespace app\modules\update\helpers;

use Yii;
use app\components\console\Console;

class SetupHelper
{

    /**
     * Runs new migrations
     *
     * @param int $numberOfMigrations
     * @return int
     */
    public static function runMigrations($numberOfMigrations = null)
    {
        // Run DB Migration
        $migrationPath = Yii::getAlias('@app/migrations');

        if (is_dir($migrationPath)) {
            // Force migrate db without confirmation
            $result = Console::run("migrate/up $numberOfMigrations --interactive=0");
            $lines = explode(PHP_EOL, $result[1]);
            $message = $lines[count($lines)-1]; // Last line
            if ($message === "Migrated up successfully.") {
                return ['success' => 1, 'message' => $message];
            } else {
                return ['success' => 0, 'message' => $message];
            }
        }

        return ['success' => 0, 'message' => 'No such migrations directory'];
    }
}
